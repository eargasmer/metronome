var child_process = require('child_process'),
		sys = require('sys'), 
		http = require('http'),
		parse = require('url').parse,
		fs = require('fs');

var spawn = child_process.spawn;
var exec = child_process.exec;

//lance lame 
//  path : path de lame
function spawnLame(path) {
  var args = ['-b','320', '-', '-']
											
	var lame = spawn(path, args);
										
	console.log('Spawning '+path+' ' + args.join(' '));
	
	lame.stderr.on('data', function (data) {
    console.log('lame stderr: ' + data);
  });
    
  return lame;
}

//lance flac sur <file> et retourne le flux dans le stdin
//  path:path de flac
function spawnFlac(path,file)
{
    var args = ['-c','-d', file]
											
	var flac = spawn(path, args);
										
	console.log('Spawning ' + path + ' ' + args.join(' '));
	
	flac.stderr.on('data', function (data) {
    console.log('flac stderr: ' + data);
  });
    
  return flac;
}


function spawnFfmpeg(file,exitCallback) {
  var args = ['-i', file, '-f', 'mp3', '-ac', '2', '-ab', '128k', '-acodec', 'libmp3lame', 
											'pipe:1']
											
	var ffmpeg = spawn('A:\\apps\\ffmpeg\\bin\\ffmpeg', args);
										
	console.log('Spawning A:\\apps\\ffmpeg\\bin\\ffmpeg ' + args.join(' '));

	ffmpeg.on('exit', exitCallback);
	
	ffmpeg.stderr.on('data', function (data) {
    console.log('grep stderr: ' + data);
  });
    
  return ffmpeg;
}


function convertion(err,config,i,yt_url,path,res)
{
    if (!err) 
    {
        //le header (mp3)
        res.writeHead(200, {'Content-Type': 'audio/mpeg'});
        
        //on lance lame
        var lame = spawnLame(config.lame);
        //on met son stdout sur la sortie du serveur
        lame.stdout.pipe(res)
        //on lance flac
        var flac = spawnFlac(config.flac,path);
        //on 'pipe' flac vers lame
        flac.stdout.pipe(lame.stdin);

    }
    else 
    {
        i++;
        if(i > config.path.length)
        {
            console.log('not found :(');
            res.end('not a file');
        }
        else
        {
            path = config.path[i]+yt_url.replace('..','');
            fs.access(path, fs.F_OK, function(err) 
            {
                convertion(err,config,i,yt_url,path,res);
            });
        }
    }
}


//main du serveur
http.createServer(function (req, res) {

    //on verifie qu'on a les bon args en paramètre
    yt_url = parse(req.url, true).query['v'];
    if(typeof yt_url == 'undefined') {
      res.end("Specify url in 'v' query param");
      return;
    }

    //on lis la config
    var configFile = fs.readFileSync("config.json");
    var config = JSON.parse(configFile);

    var i = 0;
    path = config.path[i]+yt_url.replace('..','');
    fs.access(path, fs.F_OK, function(err) 
    {
        convertion(err,config,i,yt_url,path,res);
    });


    
}).listen(8765);