Métronome
=========
Serveur pour convertir un fichier audio 'on the fly' en mp3, testé sous node-js 7.4 windows et linux
 #c'est pas de l'html mais bon c'est ce qui rendait le mieux
```html
    <url>:8765/?v=<path>
```
Dépendance
----------
 * flac
 * lame

todo
----
 * calculer / approximer la taille du mp3 final pour le header
 * être compatible avec autre que le flac:
    * wav
    * ogg
    * et mp3 tous cours